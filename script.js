function filterBy(arr, type) {

    let retArr = [];
    let idx;

    for (idx = 0; idx < arr.length; idx++) {
        if (typeof arr[idx] !== type) {
            retArr[retArr.length] = arr[idx];
        }
    }
    return retArr;
}

let testArr = ['hello', 'world', 23, '23', null];

console.log(filterBy(testArr, "string"));

//forEach нужен для перебора данных масива. Он не возвращает значение.